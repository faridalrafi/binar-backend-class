# Resources
Programming 4evryone :

## Git :
  -  [Git Introduction](https://git-scm.com/book/id/v1/Memulai-Git-Tentang-Version-Control)
  -  [Git Book](https://books.goalkicker.com/GitBook/)
## Terminal and CodeEditor Cheat Sheet :
  -  [Basic Linux Command](https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners)
  -  [VIM Cheat Sheet](https://gist.github.com/ervinismu/dc438d3668dbacb04ab36c65c4fb5570)
## Data Structure and Algorithm :
  -  [Data Structure and Algorithm](https://www.geeksforgeeks.org/data-structures/)
  -  [Software Arcitecture](https://sourcemaking.com/)
  -  [S.O.L.I.D principles](https://robots.thoughtbot.com/back-to-basics-solid)
## Programming Language :
  -  [Javascript](https://gitlab.com/ervinismu/binar-backend-class/blob/master/javascript.md)
  -  [Ruby](https://gitlab.com/ervinismu/binar-backend-class/blob/master/ruby.md)