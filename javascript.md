# Resources
Resources for learning Javascript, open contribute :

### Javascript Basics
-  [Javascript Introduction | W3schools](https://www.w3schools.com/js/default.asp)
-  [Introduction to Javascript | Codecademy](https://www.codecademy.com/learn/introduction-to-javascript)
-  [Javascript | Egghead](https://egghead.io/search?query=javascript)
-  [The Javascript Language | Javascript.io](https://javascript.info/)
-  [30 Days of Javascript | Hackerrank](https://www.hackerrank.com/domains/tutorials/10-days-of-javascript)

### Nodejs Basics
-  [Nodejs Introduction | W3schools](https://www.w3schools.com/nodejs/default.asp)
-  [Nodejs for Beginners | The Net Ninja](https://www.youtube.com/watch?v=w-7RQ46RgxU&index=1&list=PL4cUxeGkcC9gcy9lrvMJ75z9maRw4byYp)

### Server
-  [Deploy Nodejs to Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)