# Resources
Resources for learning lang and framework ruby

### Ruby
-   [Book Ruby Basic](https://launchschool.com/books/ruby/read/introduction)
-   [Book Ruby OOP(Object Oriented Programming)](https://launchschool.com/books/oo_ruby/read/introduction)
-   [Ruby Code Convention](https://github.com/rubocop-hq/ruby-style-guide)
-   [Ruby Monk](https://rubymonk.com/)
-   [Hackerrank Ruby](https://www.hackerrank.com/domains/ruby)
-   [Codecademy Ruby](https://www.codecademy.com/learn/learn-ruby)
-   [Awesome Ruby Project](http://awesome-ruby.com/)
-   [IdRails](http://www.idrails.com/series)
-   [Railscasts](http://railscasts.com/)
-   [RubyHardWay](https://learnrubythehardway.org/book/)
-   [Ruby Guides](https://www.rubyguides.com/)

### Ruby on Rails
-   [Ruby on Rails Book](https://www.railstutorial.org/book)
-   [Ruby on rails api](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)
-   [FactoryBot Cheet Seat(devhints)](https://devhints.io/factory_bot)
-   [Sending Email in Rails](https://launchschool.com/blog/handling-emails-in-rails)

## Postgresql
-   [Book Postgresql](https://books.goalkicker.com/PostgreSQLBook/)
-   [Setup Postgresql](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)

## CI/CD
-   [.gitlab-ci.yml examples](https://docs.gitlab.com/ee/ci/examples/)
-   [.gitlab-ci Variables](https://docs.gitlab.com/ee/ci/variables/)

## Server
-   [Vagrant vs Docker Concept](https://www.youtube.com/watch?v=9QGkJvbLpRA)
-   [Deploy Rails to Heroku](https://devcenter.heroku.com/articles/getting-started-with-rails5)
-   [Install Vagrant Ubuntu](https://linuxize.com/post/how-to-install-vagrant-on-ubuntu-18-04/)
-   [Initial Server Setup Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04)
-   [Setup nginx ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04)
-   [Deploy Rails with Nginx and Puma](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-rails-app-with-puma-and-nginx-on-ubuntu-14-04)
-   [SSL Setup with Nginx + Certbot](https://www.digitalocean.com/community/tutorials/how-to-set-up-let-s-encrypt-with-nginx-server-blocks-on-ubuntu-16-04)